                          
<div class="htabs" style="margin-bottom:6px; margin-top:0px;">

<li><a href="<?= $C->SITE_URL ?>settings/profile" class="<?= $this->request[1]=='profile' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_profile') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/contacts" class="<?= $this->request[1]=='contacts' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_contacts') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/avatar" class="<?= $this->request[1]=='avatar' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_avatar') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/password" class="<?= $this->request[1]=='password' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_password') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/email" class="<?= $this->request[1]=='email' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_email') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/system" class="<?= $this->request[1]=='system' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_system') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/notifications" class="<?= $this->request[1]=='notifications' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_notif') ?></a></li>
<?php if( function_exists('curl_init') ) { ?>
<li><a href="<?= $C->SITE_URL ?>settings/rssfeeds" class="<?= $this->request[1]=='rssfeeds' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_rssfeeds') ?></a></li>
<?php } ?>

<li><a href="<?= $C->SITE_URL ?>settings/background" class="<?= $this->request[1]=='background' ? 'onsidenav' : '' ?>"><?= $this->lang('settings_menu_background') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/delaccount" class="<?= $this->request[1]=='delaccount' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_delaccount') ?></a></li>

<li><a href="<?= $C->SITE_URL ?>settings/connections" class="<?= $this->request[1]=='connections' ? 'onhtab' : '' ?>"><?= $this->lang('settings_menu_conn') ?></a></li>

</div>                            
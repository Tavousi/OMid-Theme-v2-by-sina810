﻿<?php
	
	$this->load_template('header_flybox.php');
	
	if( $D->type == 'image' ) {
?>
			<div class="flyboxattachment">
				<center><img src="<?= $C->IMG_URL ?>attachments/<?= $this->network->id ?>/<?= $D->att->file_preview ?>" style="width:<?= $D->att->size_preview[0] ?>px; height:<?= $D->att->size_preview[1] ?>px;" alt="<?= htmlspecialchars($D->att->title) ?>" /></center>
			</div>
			<div class="flyboxdata"><div dir="rtl">
					اندازه تصویر: <?= $D->att->size_original[0] ?>x<?= $D->att->size_original[1] ?>پیکسل,
				حجم تصویر: <?= show_filesize($D->att->filesize) ?>
				&middot;
				<a href="<?= $C->SITE_URL ?>getfile/pid:<?= $D->p->post_tmp_id ?>/tp:image/<?= htmlspecialchars($D->att->title) ?>" target="_top"><?= $this->lang('post_atchimg_ftr_dwnld') ?></a>
				&middot;
				<a href="<?= $D->p->permalink ?>" target="_top"><?= $this->lang('post_atchftr_permalink') ?></a></div></div>
			
<?php
	}
	elseif( $D->type == 'videoembed' )
	{
?>	
			<div class="flyboxattachment">
				<?= $D->att->embed_code ?>
			</div>
			<div class="flyboxdata">
				<a href="<?= $D->att->orig_url ?>" target="_blank"><?= str_cut_link($D->att->orig_url,55) ?></a> &middot;
				<a href="<?= $D->att->orig_url ?>" target="_blank"><?= $this->lang('post_atchvid_ftr_site') ?></a> &middot;
				<a href="<?= $D->p->permalink ?>" target="_top"><?= $this->lang('post_atchftr_permalink') ?></a>
			</div>
<?php	
	}
	
	$this->load_template('footer_flybox.php');
	
?>
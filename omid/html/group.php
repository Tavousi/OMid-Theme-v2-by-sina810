<?php
	
	$this->load_template('header.php');
	
?>


		<?php if( $D->i_am_member ) { ?>
		<script type="text/javascript">
			pf_hotkeyopen_loadgroup	= "<?= $D->g->title ?>";
		</script>
		<?php } ?>
		<?php if($this->param('msg')=='created') { ?>
		<?= okbox($this->lang('group_justcreated_box_ttl'), $this->lang('group_justcreated_box_txt',array('#A1#'=>'<a href="'.userlink($D->g->groupname).'/invite">','#A2#'=>'</a>','#A3#'=>'<a href="'.userlink($D->g->groupname).'/tab:settings">','#A4#'=>'</a>',)) ) ?>
		<?php } elseif($this->param('msg')=='invited') { ?>
		<?= okbox($this->lang('group_invited_box_ttl'), $this->lang('group_invited_box_txt') ) ?>
		<?php } ?>
		<div id="profile">
			<div id="profile2">
				<div id="profile_left">
					<div id="profileavatar"><img src="<?= $C->IMG_URL.'avatars/'.$D->g->avatar ?>" alt="" style="-moz-border-radius: 5px;-webkit-border-radius: 5px;" />
                    <?php if($this->user->is_logged) { ?>
						<div id="usermenu">
                        <?php if($D->tab == 'updates') { ?>
							<a href="javascript:;" onclick="postform_open(({groupname:'<?= $D->g->title ?>'}));" class="um_ptg" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');"><b><?= $this->lang('grp_toplnks_post',array('#GROUP#'=>$D->g->title)) ?></b></a><?php } ?>
							<?php if( ! $D->i_am_member ) { ?>
							<a href="<?= userlink($D->g->groupname) ?>/tab:<?= $D->tab ?>/act:join" id="grppg_btn_follow" class="um_joingr" onfocus="this.blur();" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');"><b><?= $this->lang('grp_toplnks_follow',array('#GROUP#'=>$D->g->title)) ?></b></a>
							<?php } elseif( $this->user->if_can_leave_group($D->g->id) ) { ?>
							<a href="<?= userlink($D->g->groupname) ?>/tab:<?= $D->tab ?>/act:leave" id="grppg_btn_unfollow" onclick="return confirm('<?= $this->lang('group_unfollow_confirm',array('#GROUP#'=>$D->g->groupname)) ?>');" class="um_leavegr" onfocus="this.blur();" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');"><b><?= $this->lang('grp_toplnks_unfollow',array('#GROUP#'=>$D->g->title)) ?></b></a>
							<?php } ?>
						</div>
						<?php } ?>
                    </div>
					<?php if( !empty($D->g->about_me) ) { ?>
					<p><div class="greygrad">
						<div class="greygrad2">
							<div class="greygrad3">
								<?= $D->about_me ?>
							</div>
						</div>
					</div></p>
					<?php } ?>
					<div class="ttl" style="margin-bottom:8px;">
						<div class="ttl2">
							<h3><?= $this->lang('group_left_members') ?></h3>
							<?php if( $D->num_members > 6 ) { ?>
							<a href="<?= userlink($D->g->groupname) ?>/tab:members" class="ttlink"><?= $this->lang('group_left_viewall') ?></a>
							<? } ?>
						</div>
					</div>
					<div class="slimusergroup">
						<?php foreach($D->some_members as $u) { ?>
						<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img src="<?= $C->IMG_URL ?>avatars/thumbs3/<?= $u->avatar ?>" alt="" /></a>
						<?php } ?>
					</div>
					<div class="ttl" style="margin-bottom:8px; margin-top:4px;">
						<div class="ttl2">
							<h3><?= $this->lang('group_left_admins') ?></h3>
							<?php if( $D->num_admins > 3 ) { ?>
							<a href="<?= userlink($D->g->groupname) ?>/tab:members/filter:admins" class="ttlink"><?= $this->lang('group_left_viewall') ?></a>
							<? } ?>
						</div>
					</div>
					<div class="slimusergroup">
						<?php foreach($D->some_admins as $u) { ?>
						<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img src="<?= $C->IMG_URL ?>avatars/thumbs3/<?= $u->avatar ?>" alt="" /></a>
						<?php } ?>
					</div>
					
					<?php if( count($D->post_tags) > 0 ) { ?>
					<div class="ttl" style="margin-top:5px; margin-bottom:5px;"><div class="ttl2"><h3><?= $this->lang('group_left_posttags') ?></h3></div></div>
					<div class="taglist">
						<?php foreach($D->post_tags as $tmp) { ?>
						<a href="<?= $C->SITE_URL ?>search/posttag:%23<?= $tmp ?>" title="#<?= htmlspecialchars($tmp) ?>"><small>#</small><?= htmlspecialchars(str_cut($tmp,25)) ?></a>
						<?php } ?>
					</div>
					<?php } ?>
					
					<?php if( $D->i_can_invite ) { ?>
					<div class="greygrad" style="margin-top:3px;">
						<div class="greygrad2">
							<div class="greygrad3">
								<?= $this->lang('group_left_invite_txt', array('#GROUP#'=>htmlspecialchars($D->g->title))) ?>
								<div class="klear"></div>
	<a href="<?= userlink($D->g->groupname) ?>/invite"><button class="clean-gray" style="float:left;width:90px;"><?= $this->lang('group_left_invite_btn') ?></button></a>
							</div>
						</div>
					</div>
					<?php } ?>
					
				</div>
				<div id="profile_right">
						
<?php if( $this->user->is_logged ) { ?><?php } else { ?>
<DIV id="usr-prfttl2">
<B style="font-weight: normal;">شما می توانید با وارد شدن به سایت در گروه <?= htmlspecialchars(str_cut($D->g->title,25)) ?> عضو شوید و همچنین می توانید مطالبتان را در این گروه منتشر کنید!</B>
</DIV>                
<?php } ?>                        
                        
                <DIV id="usr-prfttl">
<B style="color:#7f7e7d;font-weight: normal;"><?= htmlspecialchars(str_cut($D->g->title,25)) ?></B>
<span>
(<?= $this->lang( $D->g->is_private ? 'group_subtitle_type_private' : 'group_subtitle_type_public' )  ?> &middot;
<?= $this->lang( $D->num_members==1 ? 'group_subtitle_nm_members1' : 'group_subtitle_nm_members', array('#NUM#'=>$D->num_members) ) ?> &middot;
<?= $this->lang( $D->g->num_posts==1 ? 'group_subtitle_nm_posts1' : 'group_subtitle_nm_posts', array('#NUM#'=>$D->g->num_posts) ) ?>)
</span>
<DIV style="float:left; padding-right: 25px;">
<A href="<?= userlink($D->g->groupname) ?>" class="<?= $D->tab=='updates'?'onptab':'' ?>"> <?= $this->lang('grp_tab_updates') ?> </A> |<A href="<?= userlink($D->g->groupname) ?>/tab:members" class="<?= $D->tab=='members'?'onptab':'' ?>"> <?= $this->lang('grp_tab_members') ?> </A> <?php if( $D->i_am_admin ) { ?>| 
<A href="<?= userlink($D->g->groupname) ?>/tab:settings" class="<?= $D->tab=='settings'||$D->tab=='deletegroup'?'onptab':'' ?>"> <?= $this->lang('grp_tab_settings') ?> </A> <?php } ?>		
<A href="<?= $C->SITE_URL ?>rss/groupname:<?= $D->g->groupname ?>" target="_blank" title="<?= $this->lang('group_updates_rss_dsc',array('#GROUP#'=>$D->g->groupname)) ?>" > <IMG src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/rss.png" class="rss-icon" style="margin: -3px 10px -3px 4px;"></A>
</DIV>   </div>
                                                       
<div id="slim_msgbox" style="display:none;">
<strong id="slim_msgbox_msg"></strong>
<a href="javascript:;" onclick="msgbox_close('slim_msgbox'); this.blur();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
</div>

        <?php if( $this->param('msg') == 'join' ) { ?>
		<script type="text/javascript">
			slim_msgbox("<?= addslashes($this->lang('msg_follow_group_on',array('#GROUP#'=>$D->g->title))) ?>");
		</script>
		<?php } elseif( $this->param('msg') == 'leave' ) { ?>
		<script type="text/javascript">
			slim_msgbox("<?= addslashes($this->lang('msg_follow_group_off',array('#GROUP#'=>$D->g->title))) ?>");
		</script>
		<?php } ?>               
                
				<?php if( $D->tab == 'updates' ) { ?>
                   <?php if( $this->user->is_logged ) { ?>
				<div id="postform" style="display:none;">
					<form name="post_form" action="" method="post" onsubmit="return false;">
						<div id="pf_posting" style="display:none;">
							<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" /><b><?= $this->lang('pf_msg_posting') ?></b>
						</div>
						<div id="pf_loading" style="display:none;">
							<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" /><b><?= $this->lang('pf_msg_loading') ?></b>
						</div>
						<div id="pf_postedok" style="display:none;">
							<strong id="pf_postedok_msg"></strong>
							<a href="javascript:;" onclick="postform_topmsg_close();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
						</div>
						<div id="pf_postederror" style="display:none;">
							<strong id="pf_postederror_msg"></strong>
							<a href="javascript:;" onclick="postform_topmsg_close();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
						</div>
						<div id="pf_mainpart" style="display:none; position: relative;">
							<script type="text/javascript">
								pf_msg_max_length	= <?= $C->POST_MAX_SYMBOLS ?>;
								pf_close_confirm	= "<?= $this->lang('pf_confrm_close') ?>";
								pf_rmatch_confirm	= "<?= $this->lang('pf_confrm_rmat') ?>";
							</script>
							<div id="pfhdr">
								<div id="pfhdrleft">
									<b id="pf_title_newpost" style="font-weight: normal;"><?= $this->lang('pf_title_newmsg') ?></b>
									<b id="pf_title_edtpost" style="display:none;font-weight: normal;"><?= $this->lang('pf_title_edtmsg') ?></b>
									<div id="sharewith_user" class="pmuser" style="display:none;">
										<strong><?= $this->lang('pf_title_newmsg_usr') ?></strong> <input type="text" name="username" value="" rel="autocomplete" autocompleteoffset="0,3" autocompleteafter="d.post_form.message.focus(); postform_sharewith_user(d.post_form.username.value);" onblur="postform_bgcheck_username();" />
										<a href="javascript:;" onclick="dropdiv_open('updateoptions',-2);" onfocus="this.blur();"></a>
									</div>
									<div id="sharewith_group" class="pmuser" style="display:none;">
										<strong><?= $this->lang('pf_title_newmsg_grp') ?></strong> <input type="text" name="groupname" value="" rel="autocomplete" autocompleteoffset="0,3" autocompleteafter="d.post_form.message.focus(); postform_sharewith_group(d.post_form.groupname.value);" onblur="postform_bgcheck_groupname();" />
										<a href="javascript:;" onclick="dropdiv_open('updateoptions',-2);" onfocus="this.blur();"></a>
									</div>
									<div id="sharewith" onclick="dropdiv_open('updateoptions',-2);">
										<a href="javascript:;" id="selectedupdateoption" onfocus="this.blur();"><span defaultvalue="<?= $this->lang('os_pf_title_newmsg_all') ?>"></span><b></b></a>
										<div id="updateoptions" style="display:none;">
											<a href="javascript:;" onclick="postform_sharewith_all('<?= $this->lang('os_pf_title_newmsg_all') ?>');"><?= $this->lang('os_pf_title_newmsg_all') ?></a>
											<?php if( $this->request[0]=='user' && $this->params->user && $this->params->user!=$this->user->id && $tmp=$this->network->get_user_by_id($this->params->user) ) { ?>
											<a href="javascript:;" onclick="postform_sharewith_user('<?= htmlspecialchars($tmp->username) ?>');" onfocus="this.blur();" title="<?= htmlspecialchars($u->fullname) ?>"><?= htmlspecialchars(str_cut($tmp->username, 30)) ?></a>
											<?php } ?>
											<?php foreach($this->user->get_top_groups(10) as $g) { ?>
											<a href="javascript:;" onclick="postform_sharewith_group('<?= htmlspecialchars($g->title) ?>');" onfocus="this.blur();" title="<?= htmlspecialchars($g->title) ?>"><?= htmlspecialchars(str_cut($g->title, 30)) ?></a>
											<?php } ?>
											
											<a href="javascript:;" onclick="postform_sharewith_findgroup();"><?= $this->lang('pf_title_newmsg_mngrp') ?></a>
											
											<a href="javascript:;" onclick="postform_sharewith_finduser();" style="border-bottom:0px;"><?= $this->lang('pf_title_newmsg_mnusr') ?></a>
										</div>
									</div>
								</div>
								<div id="pfhdrright">
									<a href="javascript:;" onclick="postform_close_withconfirm();" onfocus="this.blur();"></a>
									<small><?= $this->lang('pf_cnt_symbols_bfr') ?><span id="pf_chars_counter"><?= $C->POST_MAX_SYMBOLS ?></span><?= $this->lang('pf_cnt_symbols_aftr') ?></small>
								</div>
							</div>
							<textarea style="width:100%" name="message" tabindex="1"></textarea>
							<div id="pfattach">
										<? if( $C->ATTACH_LINK_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('link', 96); this.blur();" id="attachbtn_link" tabindex="3"><b><?= $this->lang('pf_attachtab_link') ?></b></a>
								<? if( $C->ATTACH_LINK_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_link" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_link') ?></b> <em id="attachok_link_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('link');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_IMAGE_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('image', 131); this.blur();" id="attachbtn_image" tabindex="3"><b><?= $this->lang('pf_attachtab_image') ?></b></a>
								<? if( $C->ATTACH_IMAGE_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_image" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_image') ?></b> <em id="attachok_image_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('image');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_VIDEO_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('videoembed', 96); this.blur();" id="attachbtn_videoembed" tabindex="3"><b><?= $this->lang('pf_attachtab_videmb') ?></b></a>
								<? if( $C->ATTACH_VIDEO_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_videoembed" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_videmb') ?></b> <em id="attachok_videoembed_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('videoembed');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_FILE_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('file', 96); this.blur();" id="attachbtn_file" tabindex="3"><b><?= $this->lang('pf_attachtab_file') ?></b></a>
								<? if( $C->ATTACH_FILE_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_file" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_file') ?></b> <em id="attachok_file_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('file');" onfocus="this.blur();"></a></span></div>
<a href="javascript:;" onclick="postform_submit();" tabindex="2"><b id="postbtn_newpost"></b><b id="postbtn_edtpost" style="display:none;"></b></a>
  <a title="&#1575;&#1585;&#1587;&#1575;&#1604; &#1662;&#1587;&#1578;" href="javascript:;" onclick="postform_submit();">
<button style="width:60px;" class="clean-orng">&#1575;&#1585;&#1587;&#1575;&#1604;</button></a><a href="javascript:;" href="javascript: void(0)" onclick="window.open('<?= $C->SITE_URL ?>smileys', 'windowname2', 'width=540, \height=400, \directories=no, \location=no, \menubar=no, \resizable=no, \scrollbars=yes, \status=no, \toolbar=no'); return false;">
<button style="width:30px;height:30px;" class="cupid-green">:)</button></a>                                                               
							</div>
						</div><br><br><br>
						<div id="attachbox" style="display:none;">
							<div id="attachboxhdr"></div>
							<div id="attachboxcontent">
								<div id="attachboxcontent_link" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_link" defaultvalue="<?= $this->lang('pf_attachbx_ttl_link') ?>"></small>
										<input type="text" name="atch_link" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
									</div>
									<div id="attachboxcontent_link_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_link') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_image" style="display:none;">
									<div class="litetabs">
										<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
										<a href="javascript:;" onclick="postform_attachimage_tab('upl');" id="attachform_img_upl_btn" class="onlitetab" onfocus="this.blur();"><b><?= $this->lang('pf_attachimg_tabupl') ?></b></a>
										<a href="javascript:;" onclick="postform_attachimage_tab('url');" id="attachform_img_url_btn" class="" onfocus="this.blur();"><b><?= $this->lang('pf_attachimg_taburl') ?></b></a>
									</div>
									<div class="attachform">
										<div id="attachform_img_upl_div">
											<small id="attachboxtitle_image_upl" defaultvalue="<?= $this->lang('pf_attachbx_ttl_imupl') ?>"></small>
											<input type="file" name="atch_image_upl" value="" size="50" />
										</div>
										<div id="attachform_img_url_div" style="display:none;">
											<small id="attachboxtitle_image_url" defaultvalue="<?= $this->lang('pf_attachbx_ttl_imurl') ?>"></small>
											<input type="text" name="atch_image_url" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
										</div>
									</div>
									<div id="attachboxcontent_image_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_image') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_videoembed" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_videoembed" defaultvalue="<?= $this->lang('pf_attachbx_ttl_videm') ?>"></small>
										<input type="text" name="atch_videoembed" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
									</div>
									<div id="attachboxcontent_videoembed_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_videmb') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_file" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_file" defaultvalue="<?= $this->lang('pf_attachbx_ttl_file') ?>"></small>
										<input type="file" name="atch_file" value="" size="50" />
									</div>
									<div id="attachboxcontent_file_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_file') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
							</div>
							<div id="attachboxftr"></div>
						</div>
					</form>
				</div>
               	<?php } ?>
					<div class="ttl" style="margin-top:8px; margin-bottom:6px;">
						<div class="ttl2">
							<h3><?= $this->lang('group_title_updates',array('#GROUP#'=>htmlspecialchars($D->g->title))) ?></h3>
							<div id="postfilter">
								<a href="javascript:;" onclick="dropdiv_open('postfilteroptions');" id="postfilterselected" onfocus="this.blur();"><span><?= $this->lang('posts_filter_'.$D->filter) ?></span></a>
								<div id="postfilteroptions" style="display:none;">
									<a href="<?= userlink($D->g->groupname) ?>"><?= $this->lang('posts_filter_all') ?></a>
									<a href="<?= userlink($D->g->groupname) ?>/filter:links"><?= $this->lang('posts_filter_links') ?></a>
									<a href="<?= userlink($D->g->groupname) ?>/filter:images"><?= $this->lang('posts_filter_images') ?></a>
									<a href="<?= userlink($D->g->groupname) ?>/filter:videos"><?= $this->lang('posts_filter_videos') ?></a>
									<a href="<?= userlink($D->g->groupname) ?>/filter:files" style="border-bottom:0px;"><?= $this->lang('posts_filter_files') ?></a>
								</div>
								<span><?= $this->lang('posts_filter_ttl') ?></span>
							</div>		
						</div>
					</div>
					<?php if($this->param('msg')=='deletedpost') { ?>
					<?= okbox($this->lang('msg_post_deleted_ttl'), $this->lang('msg_post_deleted_txt'), TRUE, 'margin-bottom:6px;') ?>
					<?php } ?>
					<div id="userposts">
						<div id="posts_html">
							<?= $D->posts_html ?>
						</div>
					</div>
				<?php } elseif( $D->tab == 'members' ) { ?>
					<div class="htabs">
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:members/filter:all" class="<?= $D->filter=='all'?'onhtab':'' ?>"><?= $this->lang('group_tab_members_all') ?> (<?= $D->num_members ?>)</a></li>
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:members/filter:admins" class="<?= $D->filter=='admins'?'onhtab':'' ?>"><?= $this->lang('group_tab_members_admins') ?> (<?= $D->num_admins ?>)</a></li>
					</div>
					<div id="grouplist">
						<?= $D->users_html ?>
					</div>
				<?php } elseif( $D->tab == 'settings' ) { ?>
					<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_admintools.js"></script>
					<div class="htabs">
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:main" class="<?= $D->subtab=='main'?'onhtab':'' ?>"><?= $this->lang('group_sett_subtabs_main') ?></a></li>
						<?php if( function_exists('curl_init') ) { ?>
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:rssfeeds" class="<?= $D->subtab=='rssfeeds'?'onhtab':'' ?>"><?= $this->lang('group_sett_subtabs_rssfeeds') ?> (<?= $D->num_rssfeeds ?>)</a></li>
						<?php } ?>
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:admins" class="<?= $D->subtab=='admins'?'onhtab':'' ?>"><?= $this->lang('group_sett_subtabs_admins') ?> (<?= $D->num_admins ?>)</a></li>
						<?php if($D->g->is_private) { ?>
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:privmembers" class="<?= $D->subtab=='privmembers'?'onhtab':'' ?>"><?= $this->lang('group_sett_subtabs_privmembers') ?></a></li>
						<?php } ?>
						<li><a href="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:delgroup" class="<?= $D->subtab=='delgroup'?'onhtab':'' ?>"><?= $this->lang('group_sett_subtabs_delgroup') ?></a></li>
					</div>
					<?php if( $D->subtab == 'main' ) { ?>
						<?php if( $D->error ) { ?>
						<?= errorbox($this->lang('group_settings_f_err'), $this->lang($D->errmsg, array('#SITE_TITLE#'=>$C->SITE_TITLE)), TRUE, 'margin-top:5px; margin-bottom:4px;') ?>
						<?php } elseif( $D->submit ) { ?>
						<?= okbox($this->lang('group_settings_f_ok'), $this->lang('group_settings_f_oktxt'), TRUE, 'margin-top:5px; margin-bottom:4px;') ?>
						<?php } ?>
						<div class="greygrad" style="margin-top:6px;">
							<div class="greygrad2">
								<div class="greygrad3" style="padding-bottom:0px;">
									<form method="post" action="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:main" enctype="multipart/form-data">
										<table id="setform" cellspacing="5">
											<tr>
												<td width="80" class="setparam"><?= $this->lang('group_settings_f_title') ?></td>
												<td><input type="text" class="setinp" name="form_title" value="<?= htmlspecialchars($D->form_title) ?>" maxlength="30" style="width:320px; padding:3px;" /></td>
											</tr>
											<tr>
												<td class="setparam"><?= $this->lang('group_settings_f_url') ?></td>
												<td><?= $C->SITE_URL ?><input type="text" name="form_groupname" value="<?= htmlspecialchars($D->form_groupname) ?>" class="setinp" maxlength="30" style="width:120px; padding:3px; margin-left:2px;" /></td>
											</tr>
											<tr>
												<td class="setparam" valign="top"><?= $this->lang('group_settings_f_descr') ?></td>
												<td><textarea style="width:326px;" name="form_description"><?= htmlspecialchars($D->form_description) ?></textarea></td>
											</tr>
											<tr>
												<td class="setparam" valign="top"><?= $this->lang('group_settings_f_type') ?></td>
												<td>
													<label><input type="radio" name="form_type" value="public" <?= $D->form_type=='public'?'checked="checked"':'' ?> /><span><?= $this->lang('group_settings_f_tp_public') ?></span></label>
													<label><input type="radio" name="form_type" value="private" <?= $D->form_type=='private'?'checked="checked"':'' ?> /><span><?= $this->lang('group_settings_f_tp_private') ?></span></label>
												</td>
											</tr>
											<tr>
												<td class="setparam"><?= $this->lang('group_settings_f_avatar') ?></td>
												<td><input type="file" name="form_avatar" value="" class="setinp" style="padding:3px; margin-left:2px;" /></td>
											</tr>
											<tr>
												<td></td>
												<td><input type="submit" value="<?= $this->lang('group_settings_f_btn') ?>" name="sbm" style="padding:4px; font-weight:bold;" /></td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					<?php } elseif( $D->subtab == 'rssfeeds' ) { ?>
						<?php if( $this->param('msg') == 'added' ) { ?>
							<?= okbox($this->lang('group_feedsett_ok'), $this->lang('group_feedsett_ok_txt'), TRUE, 'margin-top:5px;margin-bottom:5px;') ?>
						<?php } elseif( $this->param('msg') == 'deleted' ) { ?>
							<?= okbox($this->lang('group_feedsett_ok'), $this->lang('group_feedsett_okdel_txt'), TRUE, 'margin-top:5px;margin-bottom:5px;') ?>
						<?php } ?>
						<?php if( count($D->feeds) > 0 ) { ?>
						<div class="greygrad" style="margin-top:6px;">
							<div class="greygrad2">
								<div class="greygrad3">
									<table id="setform" cellspacing="0" cellpadding="5">
										<tr>
											<td width="110" class="setparam" valign="top"><?= $this->lang('group_feedsett_feedslist') ?></td>
											<td width="400">
												<div class="groupfeedslist">
												<?php foreach($D->feeds as $f) { ?>
												<div class="groupfeed">
													<a href="<?= userlink($D->g->groupname).'/tab:settings/subtab:rssfeeds/delfeed:'.$f->id ?>" onclick="return confirm('<?= $this->lang('group_feedsett_feed_delcnf') ?>');" title="<?= $this->lang('group_feedsett_feed_delete') ?>" onfocus="this.blur();" class="grpdelbtn"></a>
													<?= htmlspecialchars(str_cut($f->feed_title,35)) ?>				
													<span><a href="<?= htmlspecialchars($f->feed_url) ?>" target="_blank"><?= htmlspecialchars(str_cut_link($f->feed_url,50)) ?></a></span>
													<?php if( !empty($f->filter_keywords) ) { ?>
													<span><?= $this->lang('group_feedsett_feed_filter') ?> <?= htmlspecialchars($f->filter_keywords) ?></span>
													<?php } ?>
												</div>
												<?php } ?>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="ttl" style="margin-top:8px; margin-bottom:6px;"><div class="ttl2"><h3><?= $this->lang('group_feedsett_f_title') ?></h3></div></div>
						<?php if( $D->error ) { ?>
							<?= errorbox($this->lang('group_feedsett_err'), $this->lang($D->errmsg), TRUE, 'margin-top:5px;margin-bottom:5px;') ?>
						<?php } elseif( $D->newfeed_auth_msg ) { ?>
							<?= msgbox($this->lang('group_feedsett_pwdreq_ttl'), $this->lang('group_feedsett_pwdreq_txt'), TRUE, 'margin-top:5px;margin-bottom:5px;') ?>
						<?php } ?>
						<div class="greygrad" style="margin-top:6px;">
							<div class="greygrad2">
								<div class="greygrad3" style="padding-bottom:0px;">
									<form method="post" action="<?= userlink($D->g->groupname).'/tab:settings/subtab:rssfeeds' ?>">
										<table id="setform" cellspacing="0" cellpadding="5">
											<tr>
												<td width="110" class="setparam"><?= $this->lang('group_feedsett_f_url') ?></td>
												<td><input type="text" class="setinp" name="newfeed_url" value="<?= htmlspecialchars($D->newfeed_url) ?>" maxlength="255" style="width:320px; padding:3px;" /></td>
											</tr>
											<?php if( $D->newfeed_auth_req ) { ?>
											<tr>
												<td class="setparam"><?= $this->lang('group_feedsett_f_usr') ?></td>
												<td><input type="text" class="setinp" name="newfeed_username" value="<?= htmlspecialchars($D->newfeed_username) ?>" autocomplete="off" maxlength="255" style="width:320px; padding:3px;" /></td>
											</tr>
											<tr>
												<td class="setparam"><?= $this->lang('group_feedsett_f_pwd') ?></td>
												<td><input type="password" class="setinp" name="newfeed_password" value="<?= htmlspecialchars($D->newfeed_password) ?>" autocomplete="off" maxlength="255" style="width:320px; padding:3px;" /></td>
											</tr>
											<?php } ?>
											<tr>
												<td style="padding-bottom:0px;" class="setparam"><?= $this->lang('group_feedsett_f_filter') ?></td>
												<td style="padding-bottom:0px;"><input type="text" class="setinp" name="newfeed_filter" value="<?= htmlspecialchars($D->newfeed_filter) ?>" maxlength="255" style="width:320px; padding:3px;" /></td>
											</tr>
											<tr>
												<td style="padding:0px;"></td>
												<td class="setparam" style="text-align:left; font-size:10px; padding-top:2px;"><?= $this->lang('group_feedsett_f_filtertxt') ?></td>
											</tr>
											<tr>
												<td></td>
												<td><input type="submit" name="sbm" value="<?= $this->lang('group_feedsett_f_submit') ?>" style="padding:4px; font-weight:bold;" /></td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					<?php } elseif( $D->subtab == 'admins' ) { ?>
						<?php if( $this->param('msg')=='admsaved' ) { ?>
						<?= okbox($this->lang('group_admsett_f_ok'), $this->lang('group_admsett_f_ok_txt'), TRUE, 'margin-top:5px; margin-bottom:4px;') ?>
						<?php } ?>
						<div class="greygrad" style="margin-top:6px;">
							<div class="greygrad2">
								<div class="greygrad3" style="padding-bottom:0px;">
									<table id="setform" cellspacing="5">
										<tr>
											<td width="110" class="setparam" valign="top"><?= $this->lang('group_admsett_f_adm') ?></td>
											<td width="400">
												<div class="groupadmins">
													<div class="addadmins"><?= $this->lang('group_admsett_f_adm_you') ?></div>
												</div>
												<div id="group_admins_list"></div>
											</td>
										</tr>
										<tr>
											<td class="setparam"><?= $this->lang('group_admsett_f_add') ?></td>
											<td>
												<input type="text" id="addadmin_inp" name="username" value="" style="width:200px;" rel="autocomplete" autocompleteoffset="0,3" />
												<input type="button" id="addadmin_btn" onclick="group_admins_add(); return false;" value="<?= $this->lang('group_admsett_f_add_btn') ?>" />
											</td>
										</tr>
										<tr>
											<td></td>
											<td>
												<form method="post" name="admform" action="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:admins">
													<input type="hidden" name="admins" value="" />
													<input type="submit" value="<?= $this->lang('group_admsett_f_btn') ?>" style="padding:4px; font-weight:bold;" />
												</form>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<script type="text/javascript">
							jserr_add_admin_invalid_user	= "<?= $this->lang('group_admsett_jserr_user1', array('#GROUP#'=>$D->g->title)) ?>";
							jserr_add_admin_not_member	= "<?= $this->lang('group_admsett_jserr_user2', array('#GROUP#'=>$D->g->title)) ?>";
							jsconfirm_admin_remove		= "<?= $this->lang('group_admsett_jscnf_del') ?>";
							js_group_members	= ",<?= implode(',', $D->jsmembers) ?>,";
							<?php foreach($D->admins as $u) { if($u->id==$this->user->id) { continue; } ?>
							group_admins_putintolist("<?= $u->username ?>");
							<?php } ?>
						</script>
					<?php } elseif( $D->subtab == 'privmembers' ) { ?>
						<?php if( $this->param('msg')=='mmbsaved' ) { ?>
						<?= okbox($this->lang('group_privmmb_f_ok'), $this->lang('group_privmmb_f_ok_txt'), TRUE, 'margin-top:5px; margin-bottom:4px;') ?>
						<?php } ?>
						<div class="greygrad" style="margin-top:6px;">
							<div class="greygrad2">
								<div class="greygrad3" style="padding-bottom:0px;">
									<?= $this->lang('group_privmmb_title') ?>
									<table id="setform" cellspacing="5">
										<tr>
											<td width="110" class="setparam" valign="top"><?= $this->lang('group_privmmb_f_curr') ?></td>
											<td width="400">
												<div class="groupadmins">
													<div class="addadmins"><?= $this->lang('group_privmmb_f_curr_you') ?></div>
													<?php foreach($D->cannot_be_removed as $u) { if($u->id==$this->user->id) { continue; } ?>
													<div class="addadmins"><?= $u->username ?></div>
													<?php } ?>
												</div>
												<div id="group_admins_list"></div>
											</td>
										</tr>
										<tr>
											<td></td>
											<td>
												<form method="post" name="admform" action="<?= userlink($D->g->groupname) ?>/tab:settings/subtab:privmembers">
													<input type="hidden" name="admins" value="" />
													<input type="submit" value="<?= $this->lang('group_privmmb_f_btn') ?>" style="padding:4px; font-weight:bold;" />
												</form>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<script type="text/javascript">
							jsconfirm_admin_remove		= "<?= $this->lang('group_privmmb_jscnf_del') ?>";
							<?php foreach($D->can_be_removed as $u) { if($u->id==$this->user->id) { continue; } ?>
							group_admins_putintolist("<?= $u->username ?>");
							<?php } ?>
						</script>
					<?php } elseif( $D->subtab == 'delgroup' ) { ?>
						<?php if( $D->error ) { ?>
						<?= errorbox($this->lang('group_del_f_err'), $this->lang($D->errmsg), TRUE, 'margin-top:5px; margin-bottom:4px;') ?>
						<?php } ?>
						<div class="greygrad" style="margin-top:6px;">
							<div class="greygrad2">
								<div class="greygrad3" style="padding-bottom:0px;">
									<form method="post" action="" onsubmit="return confirm('<?= $this->lang('group_del_f_btn_cnfrm') ?>');">
										<table id="setform" cellspacing="5">
											<?php if( $D->g->is_public ) { ?>
											<tr>
												<td class="setparam" valign="top"><?= $this->lang('group_del_f_posts') ?></td>
												<td>
													<label><input type="radio" name="postsact" value="keep" <?= $D->f_postsact=='keep'?'checked="checked"':'' ?> /><span><?= $this->lang('group_del_f_posts_keep') ?></span></label>
													<label><input type="radio" name="postsact" value="del" <?= $D->f_postsact=='del'?'checked="checked"':'' ?> /><span><?= $this->lang('group_del_f_posts_del') ?></span></label>
												</td>
											</tr>
											<?php } else { ?>
											<input type="hidden" name="postsact" value="del" />
											<?php } ?>
											<tr>
												<td class="setparam"><?= $this->lang('group_del_f_password') ?></td>
												<td><input type="password" class="setinp" name="password" value="" autocomplete="off" style="width:320px; padding:3px;" /></td>
											</tr>
											<tr>
												<td></td>
												<td><input type="submit" value="<?= $this->lang('group_del_f_btn') ?>" name="sbm" style="padding:4px; font-weight:bold;" /></td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				</div>
				<div class="klear"></div>
			</div>
		</div>
<?php
	
	$this->load_template('footer.php');
	
?>
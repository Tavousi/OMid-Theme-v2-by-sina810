<?php
	
	$this->load_template('header.php');
	
?>
<script type='text/javascript'>
	function check_text(){
		if (document.getElementById("a").value=='0' || 
			document.getElementById("b").value=='0'|| 
			document.getElementById("c").value=='0'
)  {
	alert("همه‌ی فیلدهای جستجو باید گزینش شده باشند!");
		return false;
	}
}
</script>
	<div id="settings">
			<?php if( $D->user_search_coplton || $D->user_search_copltoff ) { ?>
				<?php if($D->submit) { ?>
					<?= okbox('در این جستجو '.$D->numres.' کاربر مشخص شده‌است.','با کلیک بر روی اواتر کاربر، به پروفایل وی هدایت خواهید شد.') ?>
			<?php } ?>
				<?php }elseif($D->error){ ?>
					<?= errorbox($this->lang('st_avatat_err'), "متاسفانه نتیجه ای با معیار های انتخابی شما یافت نشد") ?>
			<?php } ?>
	<div id="settings_right" style="width: 648px;">
		<div class="ttl"><div class="ttl2"><h3>نتایج جستجو</h3></div></div>
	
			<div class="slimusergroup" style="margin: 10px 17px 5px 0;">
			<?php if( count($D->user_search_coplton) > 0 ) { ?>
					<?php foreach($D->user_search_coplton as $u) { ?>
						<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img id ="onlines" src="<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>" alt="" style="padding:1px;" /></a>
					<?php } ?>
				<?php }elseif(count($D->user_search_coplton) == 0){ ?> 
						نتایج جستجو پس از انتخاب گزینه های جستجو ظاهر خواهند شد ...
				<?php } ?>
			<?php if( count($D->user_search_copltoff) > 0 ) { ?>
				<?php foreach($D->user_search_copltoff as $u) { ?>
					<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img id ="offlines"src="<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>" alt="" style="padding:1px;" /></a>
				<?php } ?>
			<?php } ?></div>
			<?php if( $D->user_search_coplton || $D->user_search_copltoff ) { ?>
			<?php } ?>
		</div>
		<div id="settings_left" style="width: 280px;">
			<div class="ttl"><div class="ttl2"><h3>گزینه های جستجو</h3></div></div>
				<form method="post" action="">
					<table id="setform" cellspacing="5" style="margin-right: 10px;">
						<tr>
							<td class="setparam" style="width: 60px;">جنسیت :</td>
							<td>
								<select name="user_search_mf" id="a" style="width: 160px; font: 12px tahoma;">
									<?php foreach($D->menu_user_search_mf as $k=>$v) { ?><option value="<?= $k ?>"><?= $v ?></option><?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="setparam">سن :</td>
							<td>
								<select name="user_search_age" id="b" style="width: 160px; font: 12px tahoma;">
									<?php foreach($D->menu_user_search_age as $k=>$v) { ?><option value="<?= $k ?>"><?= $v ?></option><?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="setparam">اواتر :</td>
							<td>
								<select name="user_search_havatar" id="c"  style="width: 160px; font: 12px tahoma;" >
									<?php foreach($D->menu_user_search_havatar as $k=>$v) { ?><option value="<?= $k ?>"><?= $v ?></option><?php } ?>
								</select>
							</td>
						</tr>
                        
                        						<tr>
							<td class="setparam" style="width: 60px;">موقعیت :</td>
							<td><input style=""type="text" name="user_search_city" value="" id="d" maxlength="255" style="width: 150px;" /></td>
						</tr>
						<tr>						
							<td class="setparam">نام کامل :</td>
							<td><input style=""type="text" name="user_search_fullname" value="" id="e" maxlength="255" style="width: 150px;" /></td>
						</tr>
						<tr>
							<td class="setparam">نام کاربری :</td>
							<td><input style=""type="text" name="user_search_username" value="" id="f" maxlength="255" style="width: 150px;" dir="ltr" /></td>
						</tr>
						
                        
						<tr>
							<td></td>
							<td><input id="signin_submit" onClick="return check_text();" type="submit" name="sbm" value="جستجو" style="padding:10px; margin: 0px; font-weight:bold;"/></td>
						</tr>
					</table>
				

					
				</form>
		</div>
	</div>				
<?php
	
	$this->load_template('footer.php');
	
?>
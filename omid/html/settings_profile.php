<?php
	
	$this->load_template('header.php');
	
?>
					<div id="settings">

							<?php $this->load_template('settings_leftmenu.php') ?>
						<div>
							<?php if($D->submit) { ?>
							<?= okbox($this->lang('st_profile_ok'), $this->lang('st_profile_okmsg')) ?>
							<?php } ?>
							<div class="ttl"><div class="ttl2">
								<h3><?= $this->lang('settings_profile_ttl2') ?></h3>
								<a class="ttlink" href="<?= $C->SITE_URL ?><?= $this->user->info->username ?>/tab:info"><?= $this->lang('settings_viewprofile_link') ?></a>
							</div></div>
							<form method="post" action="">
<div style="float:right">
								<table id="setform" cellspacing="5">
									<tr>
										<td class="setparam"><?= $this->lang('st_profile_name') ?></td>
										<td><input type="text" name="name" value="<?= htmlspecialchars($D->name) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_state_lable') ?></td>
											<td><select name="state" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_1')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_2')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_3')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_3') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_4')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_4') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_4') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_5')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_5') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_5') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_6')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_6') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_6') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_7')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_7') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_7') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_8')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_8') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_8') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_9')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_9') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_9') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_10')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_10') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_10') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_11')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_11') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_11') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_12')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_12') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_12') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_13')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_13') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_13') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_14')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_14') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_14') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_15')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_15') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_15') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_16')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_16') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_16') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_17')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_17') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_17') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_18')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_18') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_18') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_19')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_19') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_19') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_20')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_20') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_20') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_21')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_21') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_21') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_22')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_22') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_22') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_23')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_23') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_23') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_24')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_24') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_24') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_25')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_25') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_25') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_26')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_26') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_26') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_27')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_27') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_27') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_28')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_28') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_28') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_29')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_29') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_29') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_30')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_30') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_30') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_state_lable_31')) ?>"<?= $D->state== $this->lang('adv_profile_state_lable_31') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_state_lable_31') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam"><?= $this->lang('st_profile_location') ?></td>
										<td><input type="text" name="location" value="<?= htmlspecialchars($D->location) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam"><?= $this->lang('st_profile_birthdate') ?></td>
										<td>
											<select name="bdate_d" class="setselect" style="width:55px;">
											<?php foreach($D->menu_bdate_d as $k=>$v) { ?>
											<option value="<?= $k ?>"<?= $k==$D->bdate_d?' selected="selected"':'' ?>><?= $v ?></option>
											<?php } ?>
											</select>
											<select name="bdate_m" class="setselect" style="width:130px;">
											<?php foreach($D->menu_bdate_m as $k=>$v) { ?>
											<option value="<?= $k ?>"<?= $k==$D->bdate_m?' selected="selected"':'' ?>><?= $v ?></option>
											<?php } ?>
											</select>
											<select name="bdate_y" class="setselect" style="width:70px;">
											<?php foreach($D->menu_bdate_y as $k=>$v) { ?>
											<option value="<?= $k ?>"<?= $k==$D->bdate_y?' selected="selected"':'' ?>><?= $v ?></option>
											<?php } ?>
											</select>
										</td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('st_profile_gender') ?></td>
										<td>
											<label><input type="radio" name="gender" value="m" <?= $D->gender=='m'?'checked="checked"':'' ?> /> <span><?= $this->lang('st_profile_gender_m') ?></span></label>
											<label><input type="radio" name="gender" value="f" <?= $D->gender=='f'?'checked="checked"':'' ?> /> <span><?= $this->lang('st_profile_gender_f') ?></span></label>
										</td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_religion_lable') ?></td>
											<td><select name="religion" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_religion_lable_1')) ?>"<?= $D->religion== $this->lang('adv_profile_religion_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_religion_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_religion_lable_2')) ?>"<?= $D->religion== $this->lang('adv_profile_religion_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_religion_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_religion_lable_3')) ?>"<?= $D->religion== $this->lang('adv_profile_religion_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_religion_lable_3') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_religion_lable_4')) ?>"<?= $D->religion== $this->lang('adv_profile_religion_lable_4') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_religion_lable_4') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_marital_lable') ?></td>
											<td><select name="marital" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_marital_lable_1')) ?>"<?= $D->marital== $this->lang('adv_profile_marital_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_marital_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_marital_lable_2')) ?>"<?= $D->marital== $this->lang('adv_profile_marital_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_marital_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_marital_lable_3')) ?>"<?= $D->marital== $this->lang('adv_profile_marital_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_marital_lable_3') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_marital_lable_4')) ?>"<?= $D->marital== $this->lang('adv_profile_marital_lable_4') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_marital_lable_4') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_marital_lable_5')) ?>"<?= $D->marital== $this->lang('adv_profile_marital_lable_5') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_marital_lable_5') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_weight_lable') ?></td>
										<td><input type="text" name="weight" value="<?= htmlspecialchars($D->weight) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_length_lable') ?></td>
										<td><input type="text" name="length" value="<?= htmlspecialchars($D->length) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_edu_lable') ?></td>
											<td><select name="edu" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_edu_lable_1')) ?>"<?= $D->edu== $this->lang('adv_profile_edu_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_edu_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_edu_lable_2')) ?>"<?= $D->edu== $this->lang('adv_profile_edu_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_edu_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_edu_lable_3')) ?>"<?= $D->edu== $this->lang('adv_profile_edu_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_edu_lable_3') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_edu_lable_4')) ?>"<?= $D->edu== $this->lang('adv_profile_edu_lable_4') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_edu_lable_4') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_edu_lable_5')) ?>"<?= $D->edu== $this->lang('adv_profile_edu_lable_5') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_edu_lable_5') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_edu_lable_6')) ?>"<?= $D->edu== $this->lang('adv_profile_edu_lable_6') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_edu_lable_6') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_job_lable') ?></td>
										<td><input type="text" name="job" value="<?= htmlspecialchars($D->job) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam"><?= $this->lang('adv_profile_umail_lable') ?></td>
										<td><input type="text" name="umail" value="<?= htmlspecialchars($D->umail) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_sold_lable') ?></td>
											<td><select name="sold" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sold_lable_1')) ?>"<?= $D->sold== $this->lang('adv_profile_sold_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sold_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sold_lable_2')) ?>"<?= $D->sold== $this->lang('adv_profile_sold_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sold_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sold_lable_3')) ?>"<?= $D->sold== $this->lang('adv_profile_sold_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sold_lable_3') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sold_lable_4')) ?>"<?= $D->sold== $this->lang('adv_profile_sold_lable_4') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sold_lable_4') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sold_lable_5')) ?>"<?= $D->sold== $this->lang('adv_profile_sold_lable_5') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sold_lable_5') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sold_lable_6')) ?>"<?= $D->sold== $this->lang('adv_profile_sold_lable_6') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sold_lable_6') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_smoke_lable') ?></td>
											<td><select name="smoke" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_smoke_lable_1')) ?>"<?= $D->smoke== $this->lang('adv_profile_smoke_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_smoke_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_smoke_lable_2')) ?>"<?= $D->smoke== $this->lang('adv_profile_smoke_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_smoke_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_smoke_lable_3')) ?>"<?= $D->smoke== $this->lang('adv_profile_smoke_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_smoke_lable_3') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam"><?= $this->lang('adv_profile_phone_lable') ?></td>
										<td><input type="text" name="phone" value="<?= htmlspecialchars($D->phone) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam"><?= $this->lang('adv_profile_car_lable') ?></td>
										<td><input type="text" name="car" value="<?= htmlspecialchars($D->car) ?>" class="setinp" maxlength="255" /></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_sense_lable') ?></td>
											<td><select name="sense" class="setselect" >
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_1')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_1') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_1') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_2')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_2') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_2') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_3')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_3') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_3') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_4')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_4') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_4') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_5')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_5') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_5') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_6')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_6') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_6') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_7')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_7') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_7') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_8')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_8') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_8') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_9')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_9') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_9') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_10')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_10') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_10') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_11')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_11') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_11') ?></option>
											<option value="<?= htmlspecialchars($this->lang('adv_profile_sense_lable_12')) ?>"<?= $D->sense== $this->lang('adv_profile_sense_lable_12') ?'selected="selected"':'' ?>><?= $this->lang('adv_profile_sense_lable_12') ?></option>
											</select></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('adv_profile_favs_lable') ?></td>
										<td><textarea name="favs" class="setinp" style="height:90px;"><?= htmlspecialchars($D->favs) ?></textarea></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('st_profile_aboutme') ?></td>
										<td><textarea name="aboutme" class="setinp" style="height:90px;"><?= htmlspecialchars($D->aboutme) ?></textarea></td>
									</tr>
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('st_profile_tags') ?></td>
										<td><textarea name="tags" class="setinp"><?= htmlspecialchars($D->tags) ?></textarea></td>
									</tr>
									<tr>
										<td></td>
										<td><button type="submit" name="sbm" value="<?= $this->lang('st_profile_savebtn') ?>" style="width:70px;padding:4px; font-weight:bold;" class="clean-gray"><b>&#1584;&#1582;&#1740;&#1585;&#1607;</b></button>
</td>
									</tr>
								</table>
</div>
<div style="float:left">
</div>

							</form>
					</div>
<?php
	
	$this->load_template('footer.php');
	
?>
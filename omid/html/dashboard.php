﻿<?php
	
	$this->load_template('header.php');
	
?>

 <script src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/facebox.js" type="text/javascript"></script>
 
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : '<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.b.gif',
        closeImage   : '<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/closelabel.png'
      })
    })
  </script>

<script type="text/javascript">
function toggle(element) {
    document.getElementById(element).style.display = (document.getElementById(element).style.display == "none") ? "" : "none";
}
</script>

					<div id="home_left">
						
						<div id="homefltr">
							<a href="<?= $C->SITE_URL ?>dashboard" class="item mystr<?= $D->tab=='all'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_all') ?></strong><span><small id="dbrd_tab_all" style="<?= $D->tabs_state['all']==0||$D->tab=='all'?'display:none;':'' ?>"><?= $D->tabs_state['all'] ?></small></span></a>
							<a href="<?= $C->SITE_URL ?>dashboard/tab:@me" class="item atme<?= $D->tab=='@me'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_@me', array('#USERNAME#'=>$this->user->info->username)) ?></strong><span><small id="dbrd_tab_mention" style="<?= $D->tabs_state['@me']==0||$D->tab=='@me'?'display:none;':'' ?>"><?= $D->tabs_state['@me'] ?></small></span></a>
							<a href="<?= $C->SITE_URL ?>dashboard/tab:private" class="item prvt<?= $D->tab=='private'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_private') ?></strong><span><small id="dbrd_tab_private" style="<?= $D->tabs_state['private']==0||$D->tab=='private'?'display:none;':'' ?>"><?= $D->tabs_state['private'] ?></small></span></a>
							<a href="<?= $C->SITE_URL ?>dashboard/tab:commented" class="item cmnt<?= $D->tab=='commented'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_commented') ?></strong><span><small id="dbrd_tab_commented" style="<?= $D->tabs_state['commented']==0||$D->tab=='commented'?'display:none;':'' ?>"><?= $D->tabs_state['commented'] ?></small></span></a>
							<?php if($D->show_feeds_tab) { ?>
								<a href="<?= $C->SITE_URL ?>dashboard/tab:feeds" class="item xfed<?= $D->tab=='feeds'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_feeds') ?></strong><span><small id="dbrd_tab_feeds" style="<?= $D->tabs_state['feeds']==0||$D->tab=='feeds'?'display:none;':'' ?>"><?= $D->tabs_state['feeds'] ?></small></span></a>
							<?php } ?>
							<?php if($D->show_twitter_tab) { ?>
								<a href="<?= $C->SITE_URL ?>dashboard/tab:tweets" class="item xtwit<?= $D->tab=='tweets'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_tweets') ?></strong><span><small id="dbrd_tab_feeds" style="<?= $D->tabs_state['tweets']==0||$D->tab=='tweets'?'display:none;':'' ?>"><?= $D->tabs_state['tweets'] ?></small></span></a>
							<?php } ?>
							<a href="<?= $C->SITE_URL ?>dashboard/tab:bookmarks" class="item fvrt<?= $D->tab=='bookmarks'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_bookmarks') ?></strong></a>
							<a href="<?= $C->SITE_URL ?>dashboard/tab:everybody" class="item allp<?= $D->tab=='everybody'?' onitem':'' ?>"><b></b><strong><?= $this->lang('dbrd_leftmenu_everybody', array('#COMPANY#'=>$C->COMPANY)) ?></strong></a>
							<?php if( count($D->menu_groups) > 0 ) { ?>
								<a href="javascript:;" id="dbrd_menu_groupsbtn" class="dropio<?= $D->groupsmenu_active?' dropped':'' ?>" onclick="dbrd_groupmenu_toggle();" onfocus="this.blur();" style="font-weight: normal;"><?= $this->lang('dbrd_leftmenu_groups') ?></a>
								<div id="dbrd_menu_groups" style="<?= $D->groupsmenu_active?'':'display:none;' ?>">
									<?php foreach($D->menu_groups as $g) { ?>

<a href="<?= $C->SITE_URL ?>dashboard/tab:group/g:<?= $g->groupname ?>" class="item<?= $D->tab=='group'&&$D->onlygroup->id==$g->id?' onitem':'' ?>" title="<?= htmlspecialchars($g->title) ?>"><b style="background-image:url('<?= $C->IMG_URL.'avatars/thumbs2/'.$g->avatar ?>');"></b><strong><?= htmlspecialchars(str_cut($g->title,20)) ?></strong></a>
									<?php } ?>
								</div>
								<script type="text/javascript">
									dbrd_grpmenu_showst	= <?= $D->groupsmenu_active ? 1 : 0 ?>;
								</script>
							<?php } ?>
						</div>
                       
                       <div style="float:left;"> 
                      <div class="text-box box-purple" style="margin-top:5px;"><center>
<A href="" rel="nofollow" target="_blank"><IMG src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/120x240.png" style="padding: 2px;"></A>
                      </center></div> 
                      <div class="text-box box-green">
<A href="http://" rel="nofollow" target="_blank">لینک اول</A>
<P id="hr"></P>
<A href="http://" rel="nofollow" target="_blank">لینک دوم</A>
<P id="hr"></P>
<A href="http://" rel="nofollow" target="_blank">لینک سوم</A>

                      </div> 
                      <div class="text-box box-orange">
<A href="http://" rel="nofollow" target="_blank">لینک اول</A>
<P id="hr"></P>
<A href="http://" rel="nofollow" target="_blank">لینک دوم</A>
<P id="hr"></P>
<A href="http://" rel="nofollow" target="_blank">لینک سوم</A>
</div> 
                      <div class="text-box box-blue">
                      
<A href="http://" rel="nofollow" target="_blank">لینک اول</A>
<P id="hr"></P>
<A href="http://" rel="nofollow" target="_blank">لینک دوم</A>
<P id="hr"></P>
<A href="http://" rel="nofollow" target="_blank">لینک سوم</A>

                      </div> 
					</div></div>
					<div id="home_content">
                    
                    				<?php if( $this->user->is_logged ) { ?>
				<div id="postform" style="display:none;">
					<form name="post_form" action="" method="post" onsubmit="return false;">
						<div id="pf_posting" style="display:none;width:510px;">
							<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" /><b><?= $this->lang('pf_msg_posting') ?></b>
						</div>
						<div id="pf_loading" style="display:none;">
							<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" /><b><?= $this->lang('pf_msg_loading') ?></b>
						</div>
						<div id="pf_postedok" style="display:none;">
							<strong id="pf_postedok_msg"></strong>
							<a href="javascript:;" onclick="postform_topmsg_close();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
						</div>
						<div id="pf_postederror" style="display:none;">
							<strong id="pf_postederror_msg"></strong>
							<a href="javascript:;" onclick="postform_topmsg_close();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
						</div>
						<div id="pf_mainpart" style="display:none; position: relative;">
							<script type="text/javascript">
								pf_msg_max_length	= <?= $C->POST_MAX_SYMBOLS ?>;
								pf_close_confirm	= "<?= $this->lang('pf_confrm_close') ?>";
								pf_rmatch_confirm	= "<?= $this->lang('pf_confrm_rmat') ?>";
							</script>
							<div id="pfhdr">
								<div id="pfhdrleft">
									<b id="pf_title_newpost" style="font-weight: normal;"><?= $this->lang('pf_title_newmsg') ?></b>
									<b id="pf_title_edtpost" style="display:none;font-weight: normal;"><?= $this->lang('pf_title_edtmsg') ?></b>
									<div id="sharewith_user" class="pmuser" style="display:none;">
										<strong><?= $this->lang('pf_title_newmsg_usr') ?></strong> <input type="text" name="username" value="" rel="autocomplete" autocompleteoffset="0,3" autocompleteafter="d.post_form.message.focus(); postform_sharewith_user(d.post_form.username.value);" onblur="postform_bgcheck_username();" />
										<a href="javascript:;" onclick="dropdiv_open('updateoptions',-2);" onfocus="this.blur();"></a>
									</div>
									<div id="sharewith_group" class="pmuser" style="display:none;">
										<strong><?= $this->lang('pf_title_newmsg_grp') ?></strong> <input type="text" name="groupname" value="" rel="autocomplete" autocompleteoffset="0,3" autocompleteafter="d.post_form.message.focus(); postform_sharewith_group(d.post_form.groupname.value);" onblur="postform_bgcheck_groupname();" />
										<a href="javascript:;" onclick="dropdiv_open('updateoptions',-2);" onfocus="this.blur();"></a>
									</div>
									<div id="sharewith" onclick="dropdiv_open('updateoptions',-2);">
										<a href="javascript:;" id="selectedupdateoption" onfocus="this.blur();"><span defaultvalue="<?= $this->lang('os_pf_title_newmsg_all') ?>"></span><b></b></a>
										<div id="updateoptions" style="display:none;">
											<a href="javascript:;" onclick="postform_sharewith_all('<?= $this->lang('os_pf_title_newmsg_all') ?>');"><?= $this->lang('os_pf_title_newmsg_all') ?></a>
											<?php if( $this->request[0]=='user' && $this->params->user && $this->params->user!=$this->user->id && $tmp=$this->network->get_user_by_id($this->params->user) ) { ?>
											<a href="javascript:;" onclick="postform_sharewith_user('<?= htmlspecialchars($tmp->username) ?>');" onfocus="this.blur();" title="<?= htmlspecialchars($u->fullname) ?>"><?= htmlspecialchars(str_cut($tmp->username, 30)) ?></a>
											<?php } ?>
											<?php foreach($this->user->get_top_groups(10) as $g) { ?>
											<a href="javascript:;" onclick="postform_sharewith_group('<?= htmlspecialchars($g->title) ?>');" onfocus="this.blur();" title="<?= htmlspecialchars($g->title) ?>"><?= htmlspecialchars(str_cut($g->title, 30)) ?></a>
											<?php } ?>
											
											<a href="javascript:;" onclick="postform_sharewith_findgroup();"><?= $this->lang('pf_title_newmsg_mngrp') ?></a>
											
											<a href="javascript:;" onclick="postform_sharewith_finduser();" style="border-bottom:0px;"><?= $this->lang('pf_title_newmsg_mnusr') ?></a>
										</div>
									</div>
								</div>
								<div id="pfhdrright">
									<a href="javascript:;" onclick="postform_close_withconfirm();" onfocus="this.blur();"></a>
									<small><?= $this->lang('pf_cnt_symbols_bfr') ?><span id="pf_chars_counter"><?= $C->POST_MAX_SYMBOLS ?></span><?= $this->lang('pf_cnt_symbols_aftr') ?></small>
								</div>
							</div>
							<textarea style="width:100%" name="message" tabindex="1"></textarea>
							<div id="pfattach">
								<? if( $C->ATTACH_LINK_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('link', 96); this.blur();" id="attachbtn_link" tabindex="3"><b><?= $this->lang('pf_attachtab_link') ?></b></a>
								<? if( $C->ATTACH_LINK_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_link" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_link') ?></b> <em id="attachok_link_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('link');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_IMAGE_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('image', 131); this.blur();" id="attachbtn_image" tabindex="3"><b><?= $this->lang('pf_attachtab_image') ?></b></a>
								<? if( $C->ATTACH_IMAGE_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_image" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_image') ?></b> <em id="attachok_image_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('image');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_VIDEO_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('videoembed', 96); this.blur();" id="attachbtn_videoembed" tabindex="3"><b><?= $this->lang('pf_attachtab_videmb') ?></b></a>
								<? if( $C->ATTACH_VIDEO_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_videoembed" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_videmb') ?></b> <em id="attachok_videoembed_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('videoembed');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_FILE_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('file', 96); this.blur();" id="attachbtn_file" tabindex="3"><b><?= $this->lang('pf_attachtab_file') ?></b></a>
								<? if( $C->ATTACH_FILE_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_file" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_file') ?></b> <em id="attachok_file_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('file');" onfocus="this.blur();"></a></span></div>
<a href="javascript:;" onclick="postform_submit();" tabindex="2"><b id="postbtn_newpost"></b><b id="postbtn_edtpost" style="display:none;"></b></a>
  <a title="&#1575;&#1585;&#1587;&#1575;&#1604; &#1662;&#1587;&#1578;" href="javascript:;" onclick="postform_submit();">
<button style="width:60px;" class="clean-orng">&#1575;&#1585;&#1587;&#1575;&#1604;</button></a><a href="javascript:;" href="javascript: void(0)" onclick="window.open('<?= $C->SITE_URL ?>smileys', 'windowname2', 'width=540, \height=400, \directories=no, \location=no, \menubar=no, \resizable=no, \scrollbars=yes, \status=no, \toolbar=no'); return false;">
<button style="width:30px;height:30px;" class="cupid-green">:)</button></a>                                                               
							</div>
						</div><br><br><br>
						<div id="attachbox" style="display:none;">
							<div id="attachboxhdr"></div>
							<div id="attachboxcontent">
								<div id="attachboxcontent_link" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_link" defaultvalue="<?= $this->lang('pf_attachbx_ttl_link') ?>"></small>
										<input type="text" name="atch_link" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
									</div>
									<div id="attachboxcontent_link_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_link') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_image" style="display:none;">
									<div class="litetabs">
										<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
										<a href="javascript:;" onclick="postform_attachimage_tab('upl');" id="attachform_img_upl_btn" class="onlitetab" onfocus="this.blur();"><b><?= $this->lang('pf_attachimg_tabupl') ?></b></a>
										<a href="javascript:;" onclick="postform_attachimage_tab('url');" id="attachform_img_url_btn" class="" onfocus="this.blur();"><b><?= $this->lang('pf_attachimg_taburl') ?></b></a>
									</div>
									<div class="attachform">
										<div id="attachform_img_upl_div">
											<small id="attachboxtitle_image_upl" defaultvalue="<?= $this->lang('pf_attachbx_ttl_imupl') ?>"></small>
											<input type="file" name="atch_image_upl" value="" size="50" />
										</div>
										<div id="attachform_img_url_div" style="display:none;">
											<small id="attachboxtitle_image_url" defaultvalue="<?= $this->lang('pf_attachbx_ttl_imurl') ?>"></small>
											<input type="text" name="atch_image_url" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
										</div>
									</div>
									<div id="attachboxcontent_image_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_image') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_videoembed" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_videoembed" defaultvalue="<?= $this->lang('pf_attachbx_ttl_videm') ?>"></small>
										<input type="text" name="atch_videoembed" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
									</div>
									<div id="attachboxcontent_videoembed_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_videmb') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_file" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_file" defaultvalue="<?= $this->lang('pf_attachbx_ttl_file') ?>"></small>
										<input type="file" name="atch_file" value="" size="50" />
									</div>
									<div id="attachboxcontent_file_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_file') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
							</div>
							<div id="attachboxftr"></div>
						</div>
					</form>
				</div>
               	<?php } ?>
                
	<?php if( $D->tab == 'group' ) { ?>
						<script type="text/javascript">
							pf_hotkeyopen_loadgroup	= "<?= $D->onlygroup->title ?>";
						</script>
						<a href="javascript:;" class="npbtn" id="postform_open_button" onclick="postform_open(({groupname:'<?= $D->onlygroup->title ?>'}));" onfocus="this.blur();">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                       در انتشار آنچه باید گفت درنگ مکن ... </a>
						<?php } else { ?>

<a href="javascript:;" class="npbtn" id="postform_open_button" onclick="postform_open();" onfocus="this.blur();">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 در انتشار آنچه باید گفت درنگ مکن ... </a>
						<?php } ?>
                    	<?php if( $D->tab == 'group' ) { ?>
						<div class="ttl" style="margin-bottom:8px;">
							<div class="ttl2">
								<h3><a href="<?= $C->SITE_URL ?><?= $D->onlygroup->groupname ?>" title="<?= htmlspecialchars($D->onlygroup->title) ?>"><?= htmlspecialchars(str_cut($D->onlygroup->title, 50)) ?></a></h3>
								<div id="postfilter">
									<a href="javascript:;" onclick="dropdiv_open('postfilteroptions');" id="postfilterselected" onfocus="this.blur();"><span><?= $this->lang('posts_filter_'.$D->filter) ?></span></a>
									<div id="postfilteroptions" style="display:none;">
										<a href="<?= $C->SITE_URL ?>dashboard/tab:group/g:<?= $D->onlygroup->groupname ?>/filter:all"><?= $this->lang('posts_filter_all') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:group/g:<?= $D->onlygroup->groupname ?>/filter:links"><?= $this->lang('posts_filter_links') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:group/g:<?= $D->onlygroup->groupname ?>/filter:images"><?= $this->lang('posts_filter_images') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:group/g:<?= $D->onlygroup->groupname ?>/filter:videos"><?= $this->lang('posts_filter_videos') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:group/g:<?= $D->onlygroup->groupname ?>/filter:files" style="border-bottom:0px;"><?= $this->lang('posts_filter_files') ?></a>
									</div>
									<span><?= $this->lang('posts_filter_ttl') ?></span>
								</div>		
							</div>
						</div>
						<?php } elseif( $D->tab != 'private' ) { ?>
						<div class="ttl" style="margin-bottom:8px;">
							<div class="ttl2">
								<h3><?= $this->lang('dbrd_poststitle_'.$D->tab, array('#USERNAME#'=>$this->user->info->username, '#COMPANY#'=>htmlspecialchars($C->COMPANY))) ?></h3>
								<div id="postfilter">
									<a href="javascript:;" onclick="dropdiv_open('postfilteroptions');" id="postfilterselected" onfocus="this.blur();"><span><?= $this->lang('posts_filter_'.$D->filter) ?></span></a>
									<div id="postfilteroptions" style="display:none;">
										<a href="<?= $C->SITE_URL ?>dashboard/tab:<?= $D->tab ?>/filter:all"><?= $this->lang('posts_filter_all') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:<?= $D->tab ?>/filter:links"><?= $this->lang('posts_filter_links') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:<?= $D->tab ?>/filter:images"><?= $this->lang('posts_filter_images') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:<?= $D->tab ?>/filter:videos"><?= $this->lang('posts_filter_videos') ?></a>
										<a href="<?= $C->SITE_URL ?>dashboard/tab:<?= $D->tab ?>/filter:files" style="border-bottom:0px;"><?= $this->lang('posts_filter_files') ?></a>
									</div>
									<span><?= $this->lang('posts_filter_ttl') ?></span>
								</div>		
							</div>
						</div>
						<?php } else { ?>
						<div class="htabs" style="margin-bottom:6px; margin-top:0px;">
							<li><a href="<?= $C->SITE_URL ?>dashboard/tab:private/filter:all/privtab:all" class="<?= $D->privtab=='all'?'onhtab':'' ?>"><?= $this->lang('dbrd_privtab_subtab_all') ?></a></li>
					    	<li><a href="<?= $C->SITE_URL ?>dashboard/tab:private/filter:all/privtab:inbox" class="<?= $D->privtab=='inbox'?'onhtab':'' ?>"><?= $this->lang('dbrd_privtab_subtab_inbox') ?></a></li>
							<li><a href="<?= $C->SITE_URL ?>dashboard/tab:private/filter:all/privtab:sent" class="<?= $D->privtab=='sent'?'onhtab':'' ?>"><?= $this->lang('dbrd_privtab_subtab_sent') ?></a></li>
							<li><a href="<?= $C->SITE_URL ?>dashboard/tab:private/filter:all/privtab:usr" class="<?= $D->privtab=='usr'?'onhtab':'' ?>"><?= $this->lang('dbrd_privtab_subtab_usr') ?></a></li>
						</div>
						<div id="pmfilter" style="display:<?= $D->privtab=='usr'&&!$D->privusr?'block':'none' ?>;">
							<form name="privform" method="POST" action="javascript:;" onsubmit="privmsg_usrfilter_setusr(this.privusr_inp.value,true); return false;">
							<?= $this->lang('dbrd_privtab_usrtb_txt') ?>: <input type="text" name="privusr_inp" value="" rel="autocomplete" autocompletecallback="privmsg_usrfilter_setusr(word);" style="width:200px;height:20px;" />
							</form>
						</div>
						<div id="pmfilterok" style="display:<?= $D->privtab=='usr'&&$D->privusr?'block':'none' ?>;">
							<strong><?= $this->lang('dbrd_privtab_usrtb_txt') ?>&nbsp;</strong> <b><?= $D->privusr->username ?></b>
							<a href="javascript:;" onclick="privmsg_usrfilter_reset();" onfocus="this.blur();"><small><?= $this->lang('dbrd_privtab_usrtb_txt_x') ?></small></a>
						</div>
						<script type="text/javascript">
							var tmpfnc	= function() { try { document.privform.privusr_inp.focus(); } catch(e) {} };
							if( d.addEventListener ) {
								d.addEventListener("load", tmpfnc, false);
								w.addEventListener("load", tmpfnc, false);
							}
							else if( d.attachEvent ) {
								d.attachEvent("onload", tmpfnc);
								w.attachEvent("onload", tmpfnc);
							}
						</script>
						<?php } ?>
						<?php if($this->param('msg')=='deletedpost') { ?>
						<?= okbox($this->lang('msg_post_deleted_ttl'), $this->lang('msg_post_deleted_txt'), TRUE, 'margin-bottom:5px;') ?>
						<?php } ?>
						<div id="posts_html">
							<?= $D->posts_html ?>
						</div>
					</div>
					<div id="home_right">
						
						<?php if( $C->MOBI_DISABLED==0 ) { ?>

<a href="<?= $C->SITE_URL ?>m"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/bn_mobile.png" /></a>

						<?php } ?>
                       									
<!-- اطلاعات گسترش دهنده شیرترانیکس  -->                       
<?php if( isset($_GET['themeby']) && $_GET['themeby']=='kianit' ) { ?>
<div id="mobiad">
<strong style="font-weight: normal;">شماره موبایل طراح قالب</strong>
تلفن : 09381585940
</div><?php } ?>
<!-- اطلاعات گسترش دهنده شیرترانیکس  -->                       



<!-- تصاویر سمت راست قالب  -->
<?php if( $this->user->is_logged && $this->user->info->is_network_admin == 1 ) { ?>
<p class="style1" align="center"><a href="javascript:NewWindow ('up.php','Uploader','320','270','center','front');">
<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/uploader.jpg" style="border-width: 0"></a></p><?php } ?>
						<p class="style1" align="center"><a href="invite/parsemail">
<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/invite.jpg" style="border-width: 0"></a></p>
<p class="style1" align="center"><a href="faq">
<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/faq.jpg" style="border-width: 0"></a></p>
<!-- تصاویر سمت راست قالب  -->




<!-- کد نمایش باکس چه کارهایی مانده! -->
						<?php if($D->whattodo_active) { ?>
						<a href="javascript:;" id="closedgtd" style="display:<?= $D->whattodo_minimized?'block':'none' ?>;" onclick="dbrd_whattodo_show();" onfocus="this.blur();"><b style="font-weight:normal;"><?= $this->lang('dbrd_whattodo_title_mnm') ?></b></a>
						<div id="greentodo" style="display:<?= $D->whattodo_minimized?'none':'block' ?>;">
							<div id="greentodo2">
								<div id="gtdttl">
									<?= $this->lang('dbrd_whattodo_title') ?>
									<a href="javascript:;" title="<?= $this->lang('dbrd_whattodo_closebtn') ?>" onclick="dbrd_whattodo_hide();" onfocus="this.blur();"></a>
								</div>
								<div id="gtdlist">
									<?php $i=0; foreach($D->whattodo_links as $l) { ?>
									<a href="<?= $l[0] ?>" class="<?= $i==0 ? 'frst' : ($i==count($D->whattodo_links)-1 ? 'last' : '') ?>"><?= $this->lang($l[1]) ?></a>
									<?php $i ++; } ?>
								</div>
							</div>
						</div>
						<?php } ?>
<!-- کد نمایش باکس چه کارهایی مانده! -->



<!-- تصویر نمایش فید  -->
<p class="style1" align="center"><a href="rss/all:posts">
<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/rss-feed.png" style="border-width: 0"></a></p>
<!-- تصویر نمایش فید  -->



<!-- کد نمایش باکس کاربران برتر! -->
<div class="ttl" style="margin-top:0px; margin-bottom:8px;">
<div class="ttl2"><a href="javascript:toggle('topuser')"><h3><?= $this->lang('dbrd_topuser_title') ?></h3></div></div></a><div id="topuser" style="display: none;">
	<div class="slimusergroup" style="margin-right:1px; margin-left:3px; margin-bottom:5px;">
	<?php foreach($D->topten as $u) { ?>
	<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img src="<?= $C->IMG_URL ?>avatars/thumbs3/<?= $u->avatar ?>" alt="" style="padding:1px;" /></a>
	<?php } ?>
	</div></div>
<?php { ?>
<!-- کد نمایش باکس کاربران برتر! -->




<!-- کد نمایش باکس گروه های فعال! -->
<div class="ttl" style="margin-top:0px; margin-bottom:8px;"><div class="ttl2"><a href="javascript:toggle('active_groups')"><h3><?= $this->lang('active_groups_block') ?></h3></a></div></div><div id="active_groups" style="display: none;">
<div class="slimusergroup" style="margin-right:1px; margin-left:3px; margin-bottom:5px;"><?php foreach($D->grplar as $u) { ?>
<a href="<?= userlink($u->groupname) ?>" class="slimuser" title="<?= ($u->groupname) ?>">
<img src="<?= $C->IMG_URL ?>avatars/thumbs3/<?= $u->avatar ?>"  style="padding:1px;" /></a>
<?php } ?>
</div></div>
<?php } ?>
<!-- کد نمایش باکس گروه های فعال! -->


<!-- کد نمایش باکس آخرین آنلاینها! -->
						<?php if( count($D->last_online) > 0 ) { ?>
						<div class="ttl" style="margin-top:0px; margin-bottom:8px;"><div class="ttl2"><a href="javascript:toggle('lastonline')"><h3><?= $this->lang('dbrd_right_lastonline') ?></h3></a></div></div>
                        <div id="lastonline">
						<div class="slimusergroup" style="margin-right:1px; margin-left:3px; margin-bottom:5px;">							<?php foreach($D->last_online as $u) { ?>
							<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img src="<?= $C->IMG_URL ?>avatars/thumbs3/<?= $u->avatar ?>" alt="" style="padding:1px;" /></a>
							<?php } ?>
						</div></div>
						<?php } ?>
<!-- کد نمایش باکس آخرین آنلاینها! -->


<!-- کد نمایش تگهای ذخیره شده! -->
    	<?php if( count($D->saved_searches) > 0 ) { ?>
						<div class="ttl" style="margin-top:0px; margin-bottom:8px;"><div class="ttl2"><a href="javascript:toggle('savedsearches')"><h3><?= $this->lang('dbrd_right_savedsearches') ?></h3></a></div></div>
                        <div id="savedsearches">
						<div class="taglist" style="margin-bottom:5px;">
							<?php foreach($D->saved_searches as $id=>$tmp) { ?>
							<a href="<?= $C->SITE_URL ?>search/saved:<?= $tmp->search_key ?>" title="<?= htmlspecialchars($tmp->search_string) ?>"><?= preg_replace('/^\#/', '<small>#</small>', htmlspecialchars(str_cut($tmp->search_string,25))) ?></a>
							<?php } ?>
						</div></div>
						<?php } ?>
<!-- کد نمایش تگهای ذخیره شده! -->
  
  
  
  
<!-- کد نمایش تگهای پستهای ارسالی! -->
						<?php if( count($D->post_tags) > 0 ) { ?>
						<div class="ttl" style="margin-top:0px; margin-bottom:8px;"><div class="ttl2">
<a href="javascript:toggle('posttags')"><h3><?= $this->lang('dbrd_right_posttags') ?></h3></a></div></div>
                        <div id="posttags">
						 <div class="taglist">
							<?php foreach($D->post_tags as $tmp) { ?>
							<a href="<?= $C->SITE_URL ?>search/posttag:%23<?= $tmp ?>" title="#<?= htmlspecialchars($tmp) ?>"><?= htmlspecialchars(str_cut($tmp,25)) ?></a>
							<?php } ?>
						</div></div>
						<?php } ?>
<!-- کد نمایش تگهای پستهای ارسالی! -->




					</div>
					<div class="klear"></div>
                    
      
<?php
	
	$this->load_template('footer.php');
	
?>
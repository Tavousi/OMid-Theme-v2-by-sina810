<?

///////////////////////////////////////////////////////////////////////

// Max size file in KB, 1024KB = 1MB
$max_file_size="1024";

// Max size for all files COMBINED in KB, 1024KB = 1MB
$max_combined_size="2048";

//How many file uploads do you want to allow at a time?
$file_uploads="5";

// Use random file names? true=yes (recommended), false=use original file name. 
$random_name=true;

// File *.array Allow For Upload
$allow_types=array("jpg","gif","png","zip","rar","txt","doc");

// Path to files folder. please Change Permission This Folder 777
$folder="./../../../uploads/";

// Full url to where files are stored.
$full_url="<?= $C->SITE_URL ?>uploads/";

// Only use this variable if you wish to use full server paths.
$fullpath="";

//Use this only if you want to password protect your uploads.
$password=""; 

///////////////////////////////////////////////////////////////////////

@session_start();
error_reporting(7);
$password_md5=md5($password);
If($password) {
	If($_POST['verify_password']==true) {
		If(md5($_POST['check_password'])==$password_md5) {
			setcookie("PersianTronix",$password_md5,time()+86400);
			sleep(1); 
			header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
			exit;
		}
	}
}

$password_form="";
If($password) {
	If($_COOKIE['PersianTronix']!=$password_md5) {
		$password_form="<br><br><br><br><br><br><form method=\"POST\" action=\"".$_SERVER['PHP_SELF']."\">\n";
		$password_form.="<table align=\"center\" class=\"table\">\n";
		$password_form.="<tr>\n";
		$password_form.="<td width=\"100%\" class=\"table_header\" colspan=\"2\">ورود به سیستم</td>\n";
		$password_form.="</tr>\n";
		$password_form.="<tr>\n";
		$password_form.="<td width=\"100%\" class=\"table_body\" colspan=\"2\"><img src=\"img/enc.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">با سلام شما برای آنکه بتوانید فایل (های) خود را آپلود کنید.<br /> نیاز به داشتن رمز عبور برای ورود به سیستم دارید.<br><br><img src=\"img/warning.gif\" width=\"10\" height=\"10\" border=\"0\" align=\"absmiddle\"> در غیر این صورت بیهوده تلاش نفرمایید.</td>\n";
		$password_form.="</tr>\n";
		$password_form.="<tr>\n";
		$password_form.="<td width=\"45%\" class=\"table_body\">رمز عبور را وارد کنید:</td>\n";
		$password_form.="<td width=\"55%\" class=\"table_body\"><input type=\"password\" name=\"check_password\" /></td>\n";
		$password_form.="</tr>\n";
		$password_form.="<td colspan=\"2\" align=\"center\" class=\"table_body\">\n";
		$password_form.="<input type=\"hidden\" name=\"verify_password\" value=\"true\">\n";
		$password_form.="<input type=\"submit\" value=\" ورود \" />\n";
		$password_form.="</td>\n";
		$password_form.="</tr>\n";
		$password_form.="</table>\n";
		$password_form.="</form>\n";
	}
}
function get_ext($key) { 
	$key=strtolower(substr(strrchr($key, "."), 1));
	$key=str_replace("jpeg","jpg",$key);
	return $key;
}
$ext_count=count($allow_types);
$i=0;
foreach($allow_types AS $extension) {
	If($i <= $ext_count-2) {
		$types .="*.".$extension.", ";
	} Else {
		$types .="*.".$extension;
	}
	$i++;
}
unset($i,$ext_count);
$error="";
$display_message="";
$uploaded==false;

If($_POST['submit']==true AND !$password_form) {
	For($i=0; $i <= $file_uploads-1; $i++) {
		If($_FILES['file']['name'][$i]) {
			$ext=get_ext($_FILES['file']['name'][$i]);
			$size=$_FILES['file']['size'][$i];
			$max_bytes=$max_file_size*3072;
			If($random_name){
				$file_name[$i]=time()+rand(0,100000000).".".$ext;
			} Else {
				$file_name[$i]=$_FILES['file']['name'][$i];
			}
			If(!in_array($ext, $allow_types)) {
				$error.= "شما اجازه آپلود این پسوند را ندارید: <span dir=ltr>".$_FILES['file']['name'][$i]."</span>, فقط ".$types." مجاز هستند.<br />فایل شما آپلود نشد.<br />";
			} Elseif($size > $max_bytes) {
				$error.= "حجم فایل: ".$_FILES['file']['name'][$i]." زیاد میباشد. حداکثر مجاز  ".$max_file_size."kb.<br />فایل شما آپلود نشد<br />";
			} Elseif(file_exists($folder.$file_name[$i])) {
				$error.= "فایل: ".$_FILES['file']['name'][$i]." قبلا بر روی سرور ذخیره شده<br />فایل شما آپلود نشد<br />";
			}
		} 
	}
	$total_size=array_sum($_FILES['file']['size']);
	$max_combined_bytes=$max_combined_size*1024;
			If($total_size > $max_combined_bytes) {
				$error.="حداکثر حجم مجاز برای آپلود فایل : ".$max_combined_size."kb<br />";
			}
	If($error) {
		$display_message=$error;
	} Else {
		For($i=0; $i <= $file_uploads-1; $i++) {
			If($_FILES['file']['name'][$i]) {
				If(@move_uploaded_file($_FILES['file']['tmp_name'][$i],$folder.$file_name[$i])) {
					$uploaded=true;
				} Else {
					$display_message.="فایل ".$file_name[$i]." آپلود نشد, لطفا سطح دسترسی فولدر ".$folder." را به 777 تغییر دهید.\n";
				}
			}
		}
	}
} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Language" content="en-us" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?= htmlspecialchars($D->page_title) ?>
</title>
<? echo base64_decode('PCEtLS0gUGVyc2lhblRyb25peC5pciAtLS0+DQo8TUVUQSBOQU1FPSJMYW5ndWFnZSIgQ09OVEVO
VD0iRmFyc2kiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCmJvZHl7ZGlyZWN0aW9uOnJ0bDti
YWNrZ3JvdW5kLWNvbG9yOiNGRkZGRkY7Zm9udC1mYW1pbHk6VGFob21hO2ZvbnQtc2l6ZToxMHB4
O2NvbG9yOiMwMDAwMDA7fQ0KLmVycm9yX21lc3NhZ2V7YmFja2dyb3VuZC1wb3NpdGlvbjpjZW50
ZXI7Zm9udC1mYW1pbHk6dGFob21hO2ZvbnQtc2l6ZToxMHB4O2NvbG9yOiMwMDAwMDA7Ym9yZGVy
OjFweCBzb2xpZCAjOTk5OTk5O3dpZHRoOjU0MHB4O30NCi51cGxvYWRlZF9tZXNzYWdle2ZvbnQt
ZmFtaWx5OlRhaG9tYTtmb250LXNpemU6MTFwdDtjb2xvcjojMDAwMDAwO30NCmE6bGlua3t0ZXh0
LWRlY29yYXRpb246bm9uZTtjb2xvcjojMDAwMDAwO30NCmE6dmlzaXRlZHt0ZXh0LWRlY29yYXRp
b246bm9uZTtjb2xvcjojMDAwMDAwO30NCmE6aG92ZXJ7dGV4dC1kZWNvcmF0aW9uOm5vbmU7Y29s
b3I6IzAwMDAwMDt9DQoudGFibGV7Ym9yZGVyLWNvbGxhcHNlOmNvbGxhcHNlO2JvcmRlcjoxcHgg
c29saWQgIzk5OTk5OTt3aWR0aDo0NTBweDt9DQoudGFibGVfaGVhZGVye3RleHQtYWxpZ246Y2Vu
dGVyO2NvbG9yOiMwMDAwMDA7YmFja2dyb3VuZDp1cmwoaW1nL2J1dHRvbjAzLmdpZik7Ym9yZGVy
OjFweCBzb2xpZCAjOTk5OTk5O2ZvbnQtZmFtaWx5OlRhaG9tYTtmb250LXNpemU6MTJweDt9DQou
dXBsb2FkX2luZm97YmFja2dyb3VuZDp1cmwoaW1nL2lucHV0X2JhY2suZ2lmKSByZXBlYXQteDtm
b250LWZhbWlseTpUYWhvbWE7Zm9udC1zaXplOjhwdDtjb2xvcjojMDAwMDAwO3BhZGRpbmc6NHB4
O30NCi50YWJsZV9ib2R5e2JvcmRlcjoxcHggc29saWQgIzk5OTk5OTtiYWNrZ3JvdW5kOnVybChp
bWcvaW5wdXRfYmFjay5naWYpIHJlcGVhdC14O2ZvbnQtZmFtaWx5OlRhaG9tYTtmb250LXNpemU6
MTBweDtjb2xvcjojMDAwMDAwO3BhZGRpbmc6MnB4O30NCi50YWJsZV9mb290ZXJ7Ym9yZGVyOjFw
eCBzb2xpZCAjOTk5OTk5O2JhY2tncm91bmQ6dXJsKGltZy9pbnB1dF9iYWNrLmdpZikgcmVwZWF0
LXg7dGV4dC1hbGlnbjpjZW50ZXI7cGFkZGluZzoycHg7fQ0KaW5wdXR7Y29sb3I6IzAwMDAwMDti
YWNrZ3JvdW5kOnVybChpbWcvYnV0dG9uMDMuZ2lmKTtib3JkZXI6MXB4IHNvbGlkICM5OTk5OTk7
Zm9udC1mYW1pbHk6VGFob21hO2ZvbnQtc2l6ZToxMHB4O30NCnNlbGVjdCx0ZXh0YXJlYXtiYWNr
Z3JvdW5kOnVybChpbWcvaW5wdXRfYmFjay5naWYpIHJlcGVhdC14O2JvcmRlcjoxcHggc29saWQg
Izk5OTk5OTtmb250LWZhbWlseTpUYWhvbWE7Zm9udC1zaXplOjEwcHg7fQ0KLmNvcHlyaWdodHti
b3JkZXI6MXB4IHNvbGlkICM5OTk5OTtmb250LWZhbWlseTpUYWhvbWE7Zm9udC1zaXplOjlwdDtj
b2xvcjojMDAwMDAwO3RleHQtYWxpZ246Q2VudGVyO30NCmZvcm17cGFkZGluZzowcHg7bWFyZ2lu
OjBweDt9DQo8L3N0eWxlPg0KPCEtLS0gUGVyc2lhblRyb25peC5pciAtLS0+'); ?>
<?
If($password_form) {
	Echo $password_form;
} Elseif($uploaded==true) {?>
<table align="center"class="table">
	<tr>
		<td height="24" colspan="2" class="table_header">فایل (های) شما با موفقیت آپلود شد.</td>
	</tr>
	<tr>
	<td class="table_body">
	<br />
<?
For($i=0; $i <= $file_uploads-1; $i++) {
	
	if($_FILES['file']['name'][$i]) {
		$file=$i+1;
echo("فایل#".$file.":&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"".$full_url.$file_name[$i]."\" target=\"_blank\">".$full_url.$file_name[$i]."</a><br />آدرس#".$file.":&nbsp;&nbsp;<input type=\"text\" value=\"".$full_url.$file_name[$i]."\" size=\"70\" readonly=\"readonly\" onmouseup=\"this.select()\"><br /><br />");}
}
?>
<br /><a href="<?=$_SERVER['PHP_SELF'];?>">بازگشت</a><br />
</td>
</tr>
</table>
<? } Else { ?>
<? if($display_message){ ?>
<center><div align="center" dir="rtl" class="error_message"><?=$display_message;?></div></center><br />
<? } ?>
<form action="" method="post" enctype="multipart/form-data" name="PersianTronix">
<table align="center" class="table">
	<tr>
		<td height="24" colspan="2" class="table_header"><b><?=$websitename;?></b> </td>
	</tr>
	<tr>
		<td colspan="2" class="upload_info">
			&#1662;&#1587;&#1608;&#1606;&#1583;&#1607;&#1575;&#1740; &#1605;&#1580;&#1575;&#1586;&#58;&nbsp;<font color="#FF0000"><span dir="ltr"><?=$types?></span></font><br />
			&#1581;&#1583;&#1575;&#1705;&#1579;&#1585; &#1581;&#1580;&#1605; &#1605;&#1580;&#1575;&#1586; &#1576;&#1585;&#1575;&#1740; &#1740;&#1705; &#1601;&#1575;&#1740;&#1604;&#58;&nbsp;<font color="#FF0000"><?=$max_file_size?> &#1705;&#1740;&#1604;&#1608;&#1576;&#1575;&#1740;&#1578;</font><br />
			&#1581;&#1583;&#1575;&#1705;&#1579;&#1585; &#1581;&#1580;&#1605; &#1605;&#1580;&#1575;&#1586; &#1576;&#1585;&#1575;&#1740; &#1578;&#1605;&#1575;&#1605; &#1601;&#1575;&#1740;&#1604;&#58;&nbsp;<font color="#FF0000"><?=$max_combined_size?> &#1705;&#1740;&#1604;&#1608;&#1576;&#1575;&#1740;&#1578;</font><br />
		</td>
	</tr>
	<? For($i=0;$i <= $file_uploads-1;$i++) { 
echo base64_decode('CTx0cj4NCgkJCTx0ZCBjbGFzcz0idGFibGVfYm9keSIgd2lkdGg9IjIwJSI+JiMxNTc1OyYjMTYw
NjsmIzE1Nzg7JiMxNTgyOyYjMTU3NTsmIzE1NzY7ICYjMTYwMTsmIzE1NzU7JiMxNzQwOyYjMTYw
NDsgJiM1ODs8L3RkPg0KCQkJPHRkIGNsYXNzPSJ0YWJsZV9ib2R5IiB3aWR0aD0iODAlIj48aW5w
dXQgdHlwZT0iZmlsZSIgbmFtZT0iZmlsZVtdIiBzaXplPSIzMCIgLz48L3RkPg0KCQk8L3RyPg0K
ٹ');	
	}
echo base64_decode('CTx0cj4NCgkJPHRkIGNvbHNwYW49IjIiIGFsaWduPSJjZW50ZXIiIGNsYXNzPSJ0YWJsZV9mb290ZXIiPg0KCQkJPGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0ic3VibWl0IiB2YWx1ZT0idHJ1ZSIgLz4NCgkJCTxpbnB1dCB0eXBlPSJzdWJtaXQiIHZhbHVlPSIgJiMxNTcwOyYjMTY2MjsmIzE2MDQ7JiMxNjA4OyYjMTU4MzsgJiMxNjAxOyYjMTU3NTsmIzE3NDA7JiMxNjA0OyAmIzQwOyYjMTYwNzsmIzE1NzU7JiM0MTsgIiAvPiAmbmJzcDsNCgkJCTxpbnB1dCB0eXBlPSJyZXNldCIgbmFtZT0icmVzZXQiIHZhbHVlPSIgJiMxNTc1OyYjMTU4NjsgJiMxNjA2OyYjMTYwODsgIiAvPg0KCQk8L3RkPg0KCTwvdHI+DQo8L3RhYmxlPg0KPC9mb3JtPg0K');
}?>
<table class="table" style="border:0px;" align="center">
	<tr>
<td><br />
<div class="copyright"><a href="<?= $C->SITE_URL ?>up" target="_blank"><?= htmlspecialchars($D->page_title) ?></a>

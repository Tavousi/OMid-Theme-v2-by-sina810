<?php
	
	$this->load_template('header.php');
	
?>
					
							
	<?php if( $this->user->is_logged ) { ?>
		
		
			<?php if( count($D->most_posting_members) > 0 ) { $place = 0;  ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3>کاربران با بیشترین پست</h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->most_posting_members as $u) { ?>
					<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
			<?php if( count($D->most_commenting_members) > 0 ) { $place = 0;  ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3><?= $this->lang('competitions_user_competition2') ?></h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->most_commenting_members as $u) { ?>
					<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
			<?php if( count($D->most_commented_members) > 0 ) { $place = 0;  ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3><?= $this->lang('competitions_user_competition3') ?></h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->most_commented_members as $u) { ?>
					<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
			<?php if( count($D->get_mostfollowing_members) > 0 ) { $place = 0;  ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3><?= $this->lang('competitions_user_competition4') ?></h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->get_mostfollowing_members as $u) { ?>
					<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
			<?php if( count($D->get_mostfollowed_members) > 0 ) { $place = 0;  ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3><?= $this->lang('competitions_user_competition5') ?></h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->get_mostfollowed_members as $u) { ?>
					<a href="<?= userlink($u->username) ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $u->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
			
			<?php if( count($D->get_mostactive_groups) > 0 ) { $place = 0; ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3><?= $this->lang('competitions_group_competition1') ?></h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->get_mostactive_groups as $g) { ?>	
					<a href="<?= userlink($g->groupname) ?>" class="slimuser" title="<?= htmlspecialchars($g->groupname) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $g->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
			<?php if( count($D->get_mostfollowed_groups) > 0 ) { $place = 0;  ?>
			<div class="ttl" style="margin-top:20px; margin-bottom:10px;"><div class="ttl2"><h3><?= $this->lang('competitions_group_competition2') ?></h3></div></div>
			<div class="slimusergroup" style="margin-right:-10px; margin-bottom:5px;">
				<?php foreach($D->get_mostfollowed_groups as $g) { ?>
					<a href="<?= userlink($g->groupname) ?>" class="slimuser" title="<?= htmlspecialchars($g->groupname) ?>">
						<div style="background: url('<?= $C->IMG_URL ?>avatars/thumbs1/<?= $g->avatar ?>') no-repeat; width: 50px; height: 50px; margin-left: 10px;-moz-border-radius: 5px;-webkit-border-radius: 5px;">
							<p style="font-size: 11px; margin: 37px 0 0 0; padding: 0; width: 50px; background-color: #A3C1E3; text-align: center;">
								<?= ++$place; ?> <?= $this->lang('competitions_place') ?>
							</p>
							
						</div>
					</a>
				<?php } ?>
			</div>
			<?php } ?>
			
		<?php } ?>
		
	<div id="grouplist" class="groupspage">
	</div>
					
<?php
	
	$this->load_template('footer.php');
	
?>